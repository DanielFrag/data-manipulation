# Manipulação de Dados (item 2)

## Disposições gerais
A função solicitada corresponde a função "Run" do arquivo "main.go".   
É possível obter o resultado da execução dessa função impresso no console. Para isso basta fazer o build do projeto e rodar o executável gerado.
```
> go build
> ./data-manipulation
```
Para rodar os testes, basta executar a instrução:
```
go test ./...
```
