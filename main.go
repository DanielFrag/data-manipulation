package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"sort"

	"gitlab.com/DanielFrag/data-manipulation/client"
	"gitlab.com/DanielFrag/data-manipulation/handler"
	"gitlab.com/DanielFrag/data-manipulation/model"
)

// Run main function
func Run() []byte {
	clientAPI := client.APIClient{
		Client: &http.Client{},
		URL:    "https://storage.googleapis.com/dito-questions/events.json",
	}
	evetsData, getEventsError := clientAPI.GetEvents()
	if getEventsError != nil {
		fmt.Println("Error on get events", getEventsError)
		return nil
	}
	handledEvents := handler.HandleEventsArray(evetsData.EventsArray)
	timeLine := model.EventsConsolidated{
		Timeline: handledEvents,
	}
	sort.Sort(&timeLine)
	b := new(bytes.Buffer)
	json.NewEncoder(b).Encode(timeLine)
	return b.Bytes()
}

func main() {
	result := Run()
	fmt.Println(string(result))
}
