package client

import (
	"encoding/json"
	"errors"
	"io/ioutil"
	"net/http"

	"gitlab.com/DanielFrag/data-manipulation/model"
)

// APIClient client wrapper
type APIClient struct {
	Client *http.Client
	URL    string
}

// GetEvents request the events.json
func (c *APIClient) GetEvents() (model.Events, error) {
	var eventsBody model.Events
	if c.Client == nil {
		return eventsBody, errors.New("Client not defined")
	}
	res, err := c.Client.Get(c.URL)
	if err != nil {
		return eventsBody, err
	}
	defer res.Body.Close()
	body, err := ioutil.ReadAll(res.Body)
	jsonError := json.Unmarshal(body, &eventsBody)
	if jsonError != nil {
		return eventsBody, jsonError
	}
	return eventsBody, nil
}
