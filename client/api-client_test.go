package client

import (
	"bytes"
	"io/ioutil"
	"net/http"
	"testing"
)

var eventsExample = `{"events":[{"event":"comprou-produto","timestamp":"2016-09-22T13:57:32.2311892-03:00","custom_data":[{"key":"product_name","value":"Camisa Azul"},{"key":"transaction_id","value":"3029384"},{"key":"product_price","value":100}]},{"event":"comprou","timestamp":"2016-09-22T13:57:31.2311892-03:00","revenue":250,"custom_data":[{"key":"store_name","value":"Patio Savassi"},{"key":"transaction_id","value":"3029384"}]},{"event":"comprou-produto","timestamp":"2016-09-22T13:57:33.2311892-03:00","custom_data":[{"key":"product_price","value":150},{"key":"transaction_id","value":"3029384"},{"key":"product_name","value":"Calça Rosa"}]},{"event":"comprou-produto","timestamp":"2016-10-02T11:37:35.2300892-03:00","custom_data":[{"key":"transaction_id","value":"3409340"},{"key":"product_name","value":"Tenis Preto"},{"key":"product_price","value":120}]},{"event":"comprou","timestamp":"2016-10-02T11:37:31.2300892-03:00","revenue":120,"custom_data":[{"key":"transaction_id","value":"3409340"},{"key":"store_name","value":"BH Shopping"}]}]}`

type CustomRoundTripper struct {
	RoundTripFunc func(*http.Request) (*http.Response, error)
}

func (crt *CustomRoundTripper) RoundTrip(req *http.Request) (*http.Response, error) {
	return crt.RoundTripFunc(req)
}

func TestGetEvents(t *testing.T) {
	customRoundTripper := CustomRoundTripper{
		RoundTripFunc: func(req *http.Request) (*http.Response, error) {
			return &http.Response{
				StatusCode: 200,
				Body:       ioutil.NopCloser(bytes.NewBufferString(eventsExample)),
				Header:     make(http.Header),
			}, nil
		},
	}
	client := APIClient{
		Client: &http.Client{
			Transport: &customRoundTripper,
		},
		URL: "http://test.com",
	}
	evts, err := client.GetEvents()
	if err != nil {
		t.Errorf("Unexpected error: %v", err)
		return
	}
	if len(evts.EventsArray) != 5 {
		t.Errorf("Unexpected events array length: %d", len(evts.EventsArray))
		return
	}
}
