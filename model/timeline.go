package model

import "time"

// Product product struct data
type Product struct {
	Name  string  `json:"name"`
	Price float64 `json:"price"`
}

// ConsolidatedEventsData event data consolidated
type ConsolidatedEventsData struct {
	Timestamp     time.Time `json:"timestamp"`
	Revenue       float64   `json:"revenue"`
	TransactionID string    `json:"transaction_id"`
	StoreName     string    `json:"store_name"`
	Products      []Product `json:"products"`
}

// EventsConsolidated response data struct
type EventsConsolidated struct {
	Timeline []ConsolidatedEventsData `json:"timeline"`
}

// Len is part of sort.Interface
func (ec *EventsConsolidated) Len() int {
	return len(ec.Timeline)
}

// Swap is part of sort.Interface.
func (ec *EventsConsolidated) Swap(i, j int) {
	ec.Timeline[i], ec.Timeline[j] = ec.Timeline[j], ec.Timeline[i]
}

// Less is part of sort.Interface.
func (ec *EventsConsolidated) Less(i, j int) bool {
	return ec.Timeline[i].Timestamp.After(ec.Timeline[j].Timestamp)
}
