package model

import "time"

// EventCustomData event meta data struct
type EventCustomData struct {
	Key   string      `json:"key"`
	Value interface{} `json:"value"`
}

// EventEntryData single event struct
type EventEntryData struct {
	Event      string            `json:"event"`
	Timestamp  time.Time         `json:"timestamp"`
	Revenue    float64           `json:"revenue"`
	CustomData []EventCustomData `json:"custom_data"`
}

// Events array of events
type Events struct {
	EventsArray []EventEntryData `json:"events"`
}
