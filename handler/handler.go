package handler

import "gitlab.com/DanielFrag/data-manipulation/model"

func extractCustomDataValue(key string, customDataArray []model.EventCustomData) interface{} {
	for _, customData := range customDataArray {
		if customData.Key == key {
			return customData.Value
		}
	}
	return nil
}

func splitEvents(events []model.EventEntryData) (map[string]model.EventEntryData, map[string][]model.EventEntryData) {
	buyEvents := make(map[string]model.EventEntryData)
	buyProductEvents := make(map[string][]model.EventEntryData)
	for _, eventData := range events {
		transactionID := extractCustomDataValue("transaction_id", eventData.CustomData).(string)
		if eventData.Event == "comprou" {
			buyEvents[transactionID] = eventData
		} else {
			if buyProductEvents[transactionID] == nil {
				buyProductEvents[transactionID] = make([]model.EventEntryData, 0)
			}
			buyProductEvents[transactionID] = append(buyProductEvents[transactionID], eventData)
		}
	}
	return buyEvents, buyProductEvents
}

func getProductsConsolidatedData(buyProductEvents []model.EventEntryData) []model.Product {
	consolidatedData := make([]model.Product, 0)
	for _, buyProductEvent := range buyProductEvents {
		productName := extractCustomDataValue("product_name", buyProductEvent.CustomData).(string)
		productPrice := extractCustomDataValue("product_price", buyProductEvent.CustomData).(float64)
		consolidatedData = append(consolidatedData, model.Product{
			Name:  productName,
			Price: productPrice,
		})
	}
	return consolidatedData
}

// HandleEventsArray consolidate the events data
func HandleEventsArray(events []model.EventEntryData) []model.ConsolidatedEventsData {
	buyEvents, buyProductEvents := splitEvents(events)
	consolidatedDataList := make([]model.ConsolidatedEventsData, 0)
	for transactionID, buyEvent := range buyEvents {
		storeName := extractCustomDataValue("store_name", buyEvent.CustomData).(string)
		consolidatedDataList = append(consolidatedDataList, model.ConsolidatedEventsData{
			Timestamp:     buyEvent.Timestamp,
			Revenue:       buyEvent.Revenue,
			TransactionID: transactionID,
			StoreName:     storeName,
			Products:      getProductsConsolidatedData(buyProductEvents[transactionID]),
		})
	}
	return consolidatedDataList
}
