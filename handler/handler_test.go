package handler

import (
	"reflect"
	"testing"
	"time"

	"gitlab.com/DanielFrag/data-manipulation/model"
)

func TestExtractCustomDataValue(t *testing.T) {
	customDataArray := []model.EventCustomData{
		{
			Key:   "foo",
			Value: "bar",
		},
		{
			Key:   "sunda",
			Value: 300.0,
		},
		{
			Key:   "baz",
			Value: true,
		},
	}
	for _, item := range customDataArray {
		t.Run(item.Key, func(t *testing.T) {
			result := extractCustomDataValue(item.Key, customDataArray)
			if result == nil {
				t.Error("Key not foud")
				return
			}
		})
	}
	t.Run("key sunda value float64 300.0", func(t *testing.T) {
		result := extractCustomDataValue("sunda", customDataArray)
		val, ok := result.(float64)
		if !ok {
			t.Errorf("Invalid item returned. Type: %T", result)
			return
		}
		if val != 300 {
			t.Errorf("Wrong value %f", val)
			return
		}
	})
	t.Run("key foo value string \"bar\"", func(t *testing.T) {
		result := extractCustomDataValue("foo", customDataArray)
		val, ok := result.(string)
		if !ok {
			t.Errorf("Invalid item returned. Type: %T", result)
			return
		}
		if val != "bar" {
			t.Errorf("Wrong value %s", val)
			return
		}
	})
	t.Run("key baz value bool true", func(t *testing.T) {
		result := extractCustomDataValue("baz", customDataArray)
		val, ok := result.(bool)
		if !ok {
			t.Errorf("Invalid item returned. Type: %T", result)
			return
		}
		if !val {
			t.Errorf("Wrong value %t", val)
			return
		}
	})
}

func TestSplitEvents(t *testing.T) {
	transactionID := "3029384"
	events := []model.EventEntryData{
		{
			Event:     "comprou",
			Timestamp: time.Now(),
			Revenue:   10,
			CustomData: []model.EventCustomData{
				{
					Key:   "store_name",
					Value: "Patio Savassi",
				},
				{
					Key:   "transaction_id",
					Value: transactionID,
				},
			},
		},
		{
			Event:     "comprou-produto",
			Timestamp: time.Now(),
			Revenue:   0,
			CustomData: []model.EventCustomData{
				{
					Key:   "product_name",
					Value: "Tenis Preto",
				},
				{
					Key:   "transaction_id",
					Value: transactionID,
				},
				{
					Key:   "product_price",
					Value: 10,
				},
			},
		},
	}
	buyEvent, buyProductEvent := splitEvents(events)
	if _, ok := buyEvent[transactionID]; !ok {
		t.Error("Could not find the buy event")
		return
	}
	if len(buyProductEvent[transactionID]) != 1 {
		t.Errorf("Invalid length of buy products events: %d", len(buyProductEvent[transactionID]))
		return
	}
}

func TestGetProductsConsolidatedData(t *testing.T) {
	transactionID := "3029384"
	assertMap := map[string]float64{
		"Tenis Preto": 10,
		"Blusa":       3,
	}
	buyProductEvents := []model.EventEntryData{
		{
			Event:     "comprou-produto",
			Timestamp: time.Now(),
			Revenue:   0,
			CustomData: []model.EventCustomData{
				{
					Key:   "product_name",
					Value: "Tenis Preto",
				},
				{
					Key:   "transaction_id",
					Value: transactionID,
				},
				{
					Key:   "product_price",
					Value: assertMap["Tenis Preto"],
				},
			},
		},
		{
			Event:     "comprou-produto",
			Timestamp: time.Now(),
			Revenue:   0,
			CustomData: []model.EventCustomData{
				{
					Key:   "product_name",
					Value: "Blusa",
				},
				{
					Key:   "transaction_id",
					Value: transactionID,
				},
				{
					Key:   "product_price",
					Value: assertMap["Blusa"],
				},
			},
		},
	}
	productsData := getProductsConsolidatedData(buyProductEvents)
	for _, productData := range productsData {
		productPrice, ok := assertMap[productData.Name]
		if !ok {
			t.Error("Product not found")
		}
		if productPrice != productData.Price {
			t.Error("Invalid price")
		}
	}
}

func TestHandleEventsArray(t *testing.T) {
	timeFormat := "2006-01-02T15:04:05.0000000-07:00"
	timeOne, e1 := time.Parse(timeFormat, "2016-09-22T13:57:32.2311892-03:00")
	timeTwo, e2 := time.Parse(timeFormat, "2016-10-02T11:37:35.2300892-03:00")
	transactionID1 := "3029384"
	transactionID2 := "3409340"
	findByTransactionID := func(transactionID string, list []model.ConsolidatedEventsData) model.ConsolidatedEventsData {
		i := 0
		for ; i < len(list) && list[i].TransactionID != transactionID; i++ {
		}
		return list[i]
	}
	if e1 != nil || e2 != nil {
		t.Error("Invalid format")
		return
	}
	events := []model.EventEntryData{
		{
			Event:     "comprou-produto",
			Timestamp: timeOne,
			Revenue:   0.0,
			CustomData: []model.EventCustomData{
				{
					Key:   "product_name",
					Value: "Camisa Azul",
				},
				{
					Key:   "transaction_id",
					Value: transactionID1,
				},
				{
					Key:   "product_price",
					Value: 100.0,
				},
			},
		},
		{
			Event:     "comprou",
			Timestamp: timeOne,
			Revenue:   100.0,
			CustomData: []model.EventCustomData{
				{
					Key:   "store_name",
					Value: "Patio Savassi",
				},
				{
					Key:   "transaction_id",
					Value: transactionID1,
				},
			},
		},
		{
			Event:     "comprou-produto",
			Timestamp: timeTwo,
			Revenue:   0.0,
			CustomData: []model.EventCustomData{
				{
					Key:   "product_name",
					Value: "Bermuda",
				},
				{
					Key:   "transaction_id",
					Value: transactionID2,
				},
				{
					Key:   "product_price",
					Value: 300.0,
				},
			},
		},
		{
			Event:     "comprou-produto",
			Timestamp: timeTwo,
			Revenue:   0.0,
			CustomData: []model.EventCustomData{
				{
					Key:   "product_name",
					Value: "Calça",
				},
				{
					Key:   "transaction_id",
					Value: transactionID2,
				},
				{
					Key:   "product_price",
					Value: 300.0,
				},
			},
		},
		{
			Event:     "comprou",
			Timestamp: timeTwo,
			Revenue:   600.0,
			CustomData: []model.EventCustomData{
				{
					Key:   "store_name",
					Value: "BH shopping",
				},
				{
					Key:   "transaction_id",
					Value: transactionID2,
				},
			},
		},
	}
	expectedData := []model.ConsolidatedEventsData{
		{
			Timestamp:     timeOne,
			Revenue:       100.0,
			TransactionID: transactionID1,
			StoreName:     "Patio Savassi",
			Products: []model.Product{
				{
					Name:  "Camisa Azul",
					Price: 100.0,
				},
			},
		},
		{
			Timestamp:     timeTwo,
			Revenue:       600.0,
			TransactionID: transactionID2,
			StoreName:     "BH shopping",
			Products: []model.Product{
				{
					Name:  "Bermuda",
					Price: 300.0,
				},
				{
					Name:  "Calça",
					Price: 300.0,
				},
			},
		},
	}
	result := HandleEventsArray(events)
	if len(result) > 2 {
		t.Errorf("HandleEventsArray. Invalid length on result, %d", len(result))
	}
	for i, r := range result {
		if !reflect.DeepEqual(r, findByTransactionID(r.TransactionID, expectedData)) {
			t.Errorf("Unexpected result on index: %d", i)
		}
	}
}
